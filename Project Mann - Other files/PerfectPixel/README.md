# Pixel Perfect Demo #

Demonstrates how to tweak the camera's orthographic size to scale a low res to high res.

Tip:
* Ensure that your screen can fit the full height of the resolution that you are testing for. Else the GameView will scale to fit and maintain the desired aspect ratio. Rotate monitor to 90deg and set the GameView width to something small-is like 1000.
* You must build asset bundle before testing the overrides (1080, 1200). To see the override with the 48PPU art, you must start with the Bootstrap scene to load the asset bundle variant.