﻿using UnityEngine;
using TeamUtility.IO;
using System.Collections;
using PlayerController;

public class Ladders : MonoBehaviour
{
    private bool fall = false;
    private PolygonCollider2D ladders;

    void Awake()
    {
        ladders = GetComponent<PolygonCollider2D>();
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (CustomPlayerController.me.isGrounded() && fall)
        {
            fall = false;
            return;
        }

        if (other.CompareTag("Player"))
        {
            if (InputManager.GetAxisRaw("Vertical") > 0 || (InputManager.GetAxisRaw("Vertical") < 0) && !fall)
            {
                CustomPlayerController.me.canInput = false;
                if (other.transform.position.x != ladders.bounds.center.x)
                    other.transform.position = new Vector2(ladders.bounds.center.x, other.transform.position.y);

                CustomPlayerController.me.setCurrentState(CustomPlayerController.State.Climbing);
                CustomPlayerController.me.canInput = true;
            }

            if(CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.Climbing)
            {
                if ((InputManager.GetAxisRaw("Vertical") == -1f && InputManager.GetButton("Jump")))
                {
                    CustomPlayerController.me.setCurrentState(CustomPlayerController.State.Normal);
                    fall = true;
                }

                if ((InputManager.GetAxisRaw("Horizontal") != 0f && InputManager.GetAxisRaw("Vertical") != 1f))
                {
                    CustomPlayerController.me.setCurrentState(CustomPlayerController.State.Normal);
                }
            }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
            fall = false;

        if (other.gameObject.tag == "Player" && CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.Climbing)
        {
            CustomPlayerController.me.setCurrentState(CustomPlayerController.State.Normal);
            
        }
    }
}
