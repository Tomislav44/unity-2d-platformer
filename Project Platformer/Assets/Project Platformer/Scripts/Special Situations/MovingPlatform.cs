﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour
{
    public float speed = 0f;
    public float mark = 0f;

    public BoxCollider2D box2D;

    void Update()
    {
        transform.root.Translate(Vector3.up * speed * Time.smoothDeltaTime);
        if (mark == 0f) mark = Time.time;
        if (Time.time >= mark + 1f)
        {
            speed *= -1f;
            mark = Time.time;
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.parent = gameObject.transform;
            Vector2 pos = Vector2.zero;
            pos.x = other.transform.position.x;
            pos.y = box2D.bounds.max.y + CustomPlayerController.me.getCollSize().y/2f;
            if(other.transform.position.y > box2D.bounds.max.y)
                other.transform.position = pos;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
            other.transform.parent = null;
    }

}
