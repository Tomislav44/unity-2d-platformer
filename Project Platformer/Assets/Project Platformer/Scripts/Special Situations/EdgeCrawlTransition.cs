﻿using UnityEngine;
using System.Collections;

public class EdgeCrawlTransition : MonoBehaviour
{
    public string orientation = "";
    public Transform parentPlatform;
    public BoxCollider2D parentBox;
    public float offset = 0f;

    private bool scaleConfirm = false;

    void OnTriggerStay2D(Collider2D other)
    {
        if (orientation == "")
            Debug.Log("Orientation not defined!");

        if (other.CompareTag("Player"))
        {
            if (orientation == "left")
            {
                if (other.transform.localScale.x < 0f)
                    scaleConfirm = true;
            }

            if (orientation == "right")
            {
                if (other.transform.localScale.x > 0f)
                    scaleConfirm = true;
            }
        }

        if (other.CompareTag("Player") && scaleConfirm && CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.Crawling)
        {
            CustomPlayerController.me.CrawlToNormalTransition();
            CustomPlayerController.me._velocity = new Vector3(0f, 0f, 0f);
            CustomPlayerController.me.enabled = false;
            CustomPlayerController.me.canInput = false;
            //other.transform.position = new Vector2(transform.position.x + offset, transform.position.y);
            other.transform.position = new Vector2(transform.position.x + (32f + CustomPlayerController.me.getCollSize().x / 2f) * CustomPlayerController.me.transform.localScale.x, transform.position.y);
            other.transform.localScale = new Vector3(-other.transform.localScale.x, other.transform.localScale.y, other.transform.localScale.z);
            CustomPlayerController.me.enabled = true;
        }

        if (other.CompareTag("Player") && scaleConfirm && CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.Hanging
            && CustomPlayerController.me.isGrounded())
        {
            CustomPlayerController.me.canInput = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            CustomPlayerController.me.canInput = true;
            scaleConfirm = false;
        }
    }

}
