﻿using UnityEngine;
using System.Collections;

public class JumpTrigger : MonoBehaviour
{
    //public JumpZone zone = null;

    private float mark = 0f;

    void Update()
    {
        if (mark != 0f && Time.time >= mark + 0.17f)
        {
            CustomPlayerController.me.edgeJump = false;
            mark = 0f;
        }
    }


    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            mark = Time.time;
            CustomPlayerController.me.edgeJump = true;
            //zone.check = false;
        }
    }

}
