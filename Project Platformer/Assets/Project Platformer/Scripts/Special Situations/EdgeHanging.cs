﻿using UnityEngine;
using System.Collections;

public class EdgeHanging : MonoBehaviour
{
    public Transform hangingObj = null;
    public Transform bottomPosition = null;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.Normal)
        {
            bottomPosition.position = new Vector3(other.transform.position.x, transform.position.y - (3f/4f)*CustomPlayerController.me.getCollSize().y, 
                other.transform.position.z);
            bottomPosition.GetComponent<EdgeCollider2D>().enabled = true;
            CustomPlayerController.me.setCurrentState(CustomPlayerController.State.Hanging);
        }
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player") && CustomPlayerController.me.getCurrentState() != CustomPlayerController.State.Hanging)
        {
            bottomPosition.GetComponent<EdgeCollider2D>().enabled = false;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player") && CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.Hanging)
        {
            bottomPosition.GetComponent<EdgeCollider2D>().enabled = false;

            CustomPlayerController.me.setCurrentState(CustomPlayerController.State.Normal);
        }
    }
}
