﻿using UnityEngine;
using System.Collections;

public class InteractionPickup : MonoBehaviour
{
    public bool isPickedUp = false;

    private Rigidbody2D myRgb2D = null;
    private Collider2D myColl2D = null;

    void Awake()
    {
        myRgb2D = GetComponent<Rigidbody2D>();
        myColl2D = GetComponent<Collider2D>();
    }

    public void onInteraction(InteractingController cont)
    {
        if (isPickedUp && cont.item == myColl2D)
        {
            isPickedUp = false;

            //myColl2D.enabled = true;
            myRgb2D.isKinematic = false;
            cont.item = null;

            transform.parent = null;
            transform.localScale = new Vector2(1f, 1f);//temp fix
        }
        else if (!isPickedUp && cont.item == null)
        {
            isPickedUp = true;

            //myColl2D.enabled = false;
            myRgb2D.isKinematic = true;
            cont.item = myColl2D;

            transform.parent = cont.itemHolder;
            transform.localPosition = new Vector2(0f, 0f);
        }
    }
}
