﻿using UnityEngine;
using TeamUtility.IO;
using System.Collections;

public class InteractingController : MonoBehaviour
{
    public Transform itemHolder = null;
    public Collider2D item;

    private Collider2D other = null;

    void Awake()
    {
        itemHolder = transform.Find("ItemHolder");
    }

    void OnTriggerEnter2D(Collider2D other) { if (other.CompareTag("Interactable")) this.other = other; }
    void OnTriggerExit2D(Collider2D other) { this.other = null; }
    void OnTriggerStay2D(Collider2D other) { /*Debug.Log(other.transform.name);*/  if (other.CompareTag("Interactable") && this.other == null) this.other = other; }

    void Update()
    {
        if (InputManager.GetButtonDown("Interact"))
        {
            if(item) item.SendMessage("onInteraction", this);
            else if (other) other.SendMessage("onInteraction", this);
        }
    }
}
