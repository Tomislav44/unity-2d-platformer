﻿using UnityEngine;
using System.Collections;

public class Spike1 : MonoBehaviour
{
    public BoxCollider2D _boxCollider2D;

    void Awake()
    {
        _boxCollider2D = GetComponent<BoxCollider2D>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player"
            && 
    ((other.transform.position.y - CustomPlayerController.me.getCollSize().y / 2f >= _boxCollider2D.bounds.center.y - _boxCollider2D.bounds.extents.y / 2.0f
    && (Mathf.Round(CustomPlayerController.me._velocity.y) < 0f))
            || (Mathf.Round(CustomPlayerController.me._velocity.y) < 0f)))
        {
            CustomPlayerController.me.setVelocity(new Vector3(0f, 0f, 0f));
            Debug.Log("Spiked!");
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {

    }
    void OnTriggerStay2D(Collider2D other)
    {

    }
}
