﻿using UnityEngine;
using System.Collections;

public class Switch1 : MonoBehaviour
{
    public int doorID = 0;
    public DoorTeleport1 door = null;

    public void onInteraction(InteractingController cont)
    {
        transform.parent.parent.localScale = new Vector3(transform.parent.parent.localScale.x * (-1f), 1f, 1f);
        transform.parent.parent.position = new Vector2(transform.parent.parent.position.x + 64f * transform.parent.parent.localScale.x * (-1f)
            ,transform.parent.parent.position.y);
        door.currentOrientation = (door.currentOrientation == DoorTeleport1.orientation.left) ? DoorTeleport1.orientation.right : DoorTeleport1.orientation.left;
        door.transform.parent.parent.localScale = new Vector3(door.transform.parent.parent.localScale.x * (-1f), 1f, 1f);
        door.transform.parent.parent.position = new Vector2(door.transform.parent.parent.position.x + 64f * door.transform.parent.parent.localScale.x * (-1f)
            , door.transform.parent.parent.position.y);
    }

    void Awake()
    {
        /*DoorTeleport1[] temp = GameObject.Find("InteractionPrefab").GetComponentsInChildren<DoorTeleport1>();

        for (int i = 0; i < temp.Length; i++)
        {
            if (temp[i].id == doorID){ door = temp[i]; break;}
        }*/
    }
}
