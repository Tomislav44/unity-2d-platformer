﻿using UnityEngine;
using System.Collections;

public class DoorLevel1 : MonoBehaviour
{
    private float timeMark = 0f;
    void OnTriggerEnter2D(Collider2D other)
    {

    }
    void OnTriggerExit2D(Collider2D other)
    {

    }
    void OnTriggerStay2D(Collider2D other)
    {
        //Debug.Log(other.transform.name);
        if (other.CompareTag("Interactable") && other.transform.name.Equals("Key01"))
        {
            Destroy(other.gameObject);
            timeMark = Time.time;
        }
    }

    void Update()
    {
        if (Time.time >= timeMark + 0.5f && timeMark != 0f)
        {
            Destroy(gameObject.transform.parent.gameObject);
        }
    }
}
