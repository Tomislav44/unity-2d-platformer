﻿using UnityEngine;
using System.Collections;

public class DoorTeleport1 : MonoBehaviour
{ 
    public enum orientation { left = -1, right = 1 }
    public orientation currentOrientation = orientation.right;

    public int mark = 0;
    public int id = 0;
    public DoorTeleport1 otherDoor = null;

    public void onInteraction(InteractingController cont)
    {
        otherDoor.SendMessage("onTeleport", cont);
    }

    public void onTeleport(InteractingController cont)
    {
        cont.transform.position = transform.parent.parent.position + new Vector3(128f, 0f) * (int)currentOrientation;
        cont.transform.localScale = new Vector3((int)currentOrientation, cont.transform.localScale.y);
    }

    void Awake ()
    {
	}
	
	void Update ()
    {
	
	}
}
