﻿using UnityEngine;
using TeamUtility.IO;
using System.Collections;

public class Cannon1 : MonoBehaviour
{
    [Range(0f, 90f)]
    public float angle = 0f;
    public bool interacting = false;

    public GameObject cannon = null;
    public GameObject projectile = null;
    public Transform offsetPosition = null;
   
    private GameObject currentProjectile = null;
    private InteractingController cont = null;

    public void onInteraction(InteractingController cont)
    {
        if (!interacting)
        {
            cont.GetComponent<CustomPlayerController>().enabled = false;
            cont.GetComponent<WeaponController>().enabled = false;
            cont.GetComponent<SpriteRenderer>().enabled = false;

            GameObject.Find("PlayerRectangle").transform.position = transform.parent.transform.position + new Vector3(500f, 0f);
            GameObject.Find("PlayerRectangle").GetComponent<PlayerRectangleFollow>().enabled = false;

            cont.gameObject.transform.position = transform.parent.transform.position;
            this.cont = cont;
            interacting = true;
        }
        else
        {
            cont.GetComponent<CustomPlayerController>().enabled = true;
            cont.GetComponent<WeaponController>().enabled = true;
            cont.GetComponent<SpriteRenderer>().enabled = true;

            GameObject.Find("PlayerRectangle").transform.position = cont.transform.position;
            GameObject.Find("PlayerRectangle").GetComponent<PlayerRectangleFollow>().enabled = true;

            //cont.gameObject.transform.position = transform.parent.transform.position;
            this.cont = null;
            interacting = false;
        }
    }

    void Update ()
    {
        if (interacting && InputManager.GetAxisRaw("Vertical") != 0f && !currentProjectile)
        {
            angle = angle + (1f * InputManager.GetAxisRaw("Vertical"));
            angle = Mathf.Clamp(angle, 0f, 90f);
            cannon.transform.eulerAngles = new Vector3(0f,0f,angle);
        }

        if (interacting && InputManager.GetButtonDown("Fire") && !currentProjectile)
        {
            currentProjectile = Instantiate(projectile, offsetPosition.position, cannon.transform.rotation) as GameObject;
            currentProjectile.GetComponent<Rigidbody2D>().AddForce(currentProjectile.transform.rotation * new Vector2(1000f, 0f), ForceMode2D.Impulse);
        }

        if (currentProjectile)
        {
            GameObject.Find("PlayerRectangle").GetComponent<PlayerRectangleFollow>().player = currentProjectile.transform;
            GameObject.Find("PlayerRectangle").GetComponent<PlayerRectangleFollow>().enabled = true;
            GameObject.Find("Main Camera").GetComponent<CameraRectangleFollow>().useFixedUpdate = true;

            if (angle == 0f) { angle = 0.1f; return; }
            if(Mathf.Round(currentProjectile.GetComponent<Rigidbody2D>().velocity.y) == 0f)
            {          
                cont.GetComponent<CustomPlayerController>().enabled = true;
                cont.GetComponent<WeaponController>().enabled = true;
                cont.GetComponent<SpriteRenderer>().enabled = true;

                GameObject.Find("PlayerRectangle").transform.position = cont.transform.position;
                GameObject.Find("PlayerRectangle").GetComponent<PlayerRectangleFollow>().enabled = true;

                float newY = currentProjectile.transform.position.y - currentProjectile.GetComponent<CircleCollider2D>().radius
                    + CustomPlayerController.me.getCollSize().y / 2f;
                cont.transform.position = new Vector3(currentProjectile.transform.position.x, newY);
                GameObject.Find("PlayerRectangle").GetComponent<PlayerRectangleFollow>().player = CustomPlayerController.me.transform;
                GameObject.Find("Main Camera").GetComponent<CameraRectangleFollow>().useFixedUpdate = false;
                Destroy(currentProjectile);
                interacting = false;
            }
        }
    }
}
