﻿using UnityEngine;
using System;
using System.Collections;
using PlayerController;
using TeamUtility.IO;

public class CustomPlayerController : MonoBehaviour
{
    public static CustomPlayerController me;

    public bool canInput = true;

    // movement config
    public float runSpeed = 8f;
    public float walkSpeed = 4f;
    public float jumpHeight = 3f;
    public float gravity = -25f;
    public float groundDamping = 20f; // how fast do we change direction? higher means faster
    public float inAirDamping = 5f;

    public float climbingSpeed = 4f;
    public float crawlingSpeed = 2f;

    //current state config
    //normal state - normal running and jumping
    //hanging state - hanging from the edge of a platform
    //climbing state - climbing on the ladders or something else
    //crawling state - crawling/crouching

    public enum State
    {
        Normal,
        Hanging,
        Climbing,
        Crawling,
        SpecialJumping,
        Knockback
    };

    private State currState = State.Normal;

    [HideInInspector]
    private float normalizedHorizontalSpeed = 0;
    private float normalizedVerticalSpeed = 0;

    private CharacterController2D _controller;
    private Animator _animator;
    private RaycastHit2D _lastControllerColliderHit;
    public Vector3 _velocity;

    private bool crawlJump = false;
    private bool isFalling = false;
    private bool isRunning = true;


    public bool jump = false;

    private float markedTime = 0f;

    private BoxCollider2D thisColl;

    private Vector2 normalCollSize, normalCollOffset;
    private Vector2 crawlCollSize, crawlCollOffset;

    //Used for knockback
    public int knockbackDirection = 1;

    //Used for Edge Jump
    public bool edgeJump = false;

    void Awake()
    {
        if (!CustomPlayerController.me)
            me = this;
        else
        {
            Debug.Log("There is already an instance of this class!");
            Destroy(this);
        }
        _animator = GetComponent<Animator>();
        _controller = GetComponent<CharacterController2D>();

        thisColl = GetComponent<BoxCollider2D>();

        normalCollSize = thisColl.size;
        normalCollOffset = thisColl.offset;

        crawlCollSize = new Vector2(thisColl.size.x, thisColl.size.x);
        crawlCollOffset = new Vector2(thisColl.offset.x, crawlOffsetCalculator(normalCollSize.y, crawlCollSize.y));

        // listen to some events for illustration purposes
        _controller.onControllerCollidedEvent += onControllerCollider;
        _controller.onTriggerEnterEvent += onTriggerEnterEvent;
        _controller.onTriggerExitEvent += onTriggerExitEvent;
    }

    #region Event Listeners
    void onControllerCollider(RaycastHit2D hit)
    {
        // bail out on plain old ground hits cause they arent very interesting
        if (hit.normal.y == 1f)
            return;

        // logs any collider hits if uncommented. it gets noisy so it is commented out for the demo
        //Debug.Log( "flags: " + _controller.collisionState + ", hit.normal: " + hit.normal );
    }
    void onTriggerEnterEvent(Collider2D col)
    {
        //Debug.Log("onTriggerEnterEvent: " + col.gameObject.name);
    }
    void onTriggerExitEvent(Collider2D col)
    {
        //Debug.Log("onTriggerExitEvent: " + col.gameObject.name);
    }
    #endregion

    private float crawlOffsetCalculator(float old_sizenum, float new_sizenum)
    {
        return ((-old_sizenum) + new_sizenum) /2f;
    }

    void Update()
    {
        fallingCheck();
        switch (currState)
        {
            case State.Normal:
                NormalState();
                break;

            case State.Hanging:
                HangingState();
                break;

            case State.Climbing:
                ClimbingState();
                break;

            case State.Crawling:
                CrawlingState();
                break;

            case State.SpecialJumping:
                SpecialJumpingState();
                break;
            case State.Knockback:
                KnockBackState();
                break;
            default:
                break;
        }


    }

    private void NormalState()
    {
        if (InputManager.GetButtonDown("Run/Walk") && canInput == true)
            isRunning = !isRunning;

        //Toggle version
        /*if (InputManager.GetButton("Run/Walk") && canInput == true)
            isRunning = false;
        else
            isRunning = true;*/

        if (InputManager.GetButtonDown("Crawl") && canInput == true && _controller.isGrounded)
        {
            currState = State.Crawling;

            thisColl.size = crawlCollSize;
            thisColl.offset = crawlCollOffset;
            _controller.recalculateDistanceBetweenRays();
            _controller.totalHorizontalRays = 2;
            return;
        }

        if (_controller.isGrounded)
            _velocity.y = 0;

        if (InputManager.GetAxisRaw("Horizontal") > 0f && canInput == true)
        {
            normalizedHorizontalSpeed = 1;
            if (transform.localScale.x < 0f)
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

            if (_controller.isGrounded)
                _animator.Play(Animator.StringToHash("Run"));
        }
        else if (InputManager.GetAxisRaw("Horizontal") < 0f && canInput == true)
        {
            normalizedHorizontalSpeed = -1;
            if (transform.localScale.x > 0f)
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

            if (_controller.isGrounded)
                _animator.Play(Animator.StringToHash("Run"));
        }
        else
        {
            normalizedHorizontalSpeed = 0;

            if (_controller.isGrounded)
                _animator.Play(Animator.StringToHash("Idle"));
        }


        // we can only jump whilst grounded
        if ((_controller.isGrounded || edgeJump) && InputManager.GetButton("Jump") && canInput == true)
        {
            _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
            if (edgeJump) edgeJump = false;
            _animator.Play(Animator.StringToHash("Jump"));
        }

        //falling animation
        if (_controller.isGrounded == false && _controller.velocity.y < 0.0f)
        {
            _animator.Play(Animator.StringToHash("Fall"));
        }


        // apply horizontal speed smoothing it. dont really do this with Lerp. Use SmoothDamp or something that provides more control
        var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
        var currentSpeed = isRunning ? runSpeed : walkSpeed;
        _velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * currentSpeed, Time.deltaTime * smoothedMovementFactor);

        // apply gravity before moving
        _velocity.y += gravity * Time.deltaTime;

        
        // if holding down bump up our movement amount and turn off one way platform detection for a frame.
        // this lets us jump down through one way platforms
        if (_controller.isGrounded && InputManager.GetAxisRaw("Vertical") < 0f && Mathf.Abs(_velocity.y) <= 40f && canInput == true)
        {
            
            _velocity.y *= 3f;
            _controller.ignoreOneWayPlatformsThisFrame = true;
        }

        _controller.move(_velocity * Time.deltaTime);

        // grab our current _velocity to use as a base for all calculations
        _velocity = _controller.velocity;
    }
    private void HangingState()
    {
        if (InputManager.GetAxisRaw("Horizontal") < 0f && transform.localScale.x > 0 && canInput == true)
        {
            normalizedHorizontalSpeed = -1;
            if (transform.localScale.x > 0f)
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        }
        else if (InputManager.GetAxisRaw("Horizontal") > 0f && transform.localScale.x < 0 && canInput == true)
        {
            normalizedHorizontalSpeed = 1;
            if (transform.localScale.x < 0f)
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        }
        else
        {
            normalizedHorizontalSpeed = 0;

            if (_controller.isGrounded)
                _animator.Play(Animator.StringToHash("Hang"));
        }

        // we can only jump whilst grounded
        if (_controller.isGrounded && InputManager.GetButton("Jump") && canInput == true)
        {
            _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
            _controller.ignoreOneWayPlatformsThisFrame = true;//fix for edges near ladders
            _animator.Play(Animator.StringToHash("Jump"));
        }

        // apply horizontal speed smoothing it. dont really do this with Lerp. Use SmoothDamp or something that provides more control
        var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
        _velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor);

        // apply gravity before moving
        _velocity.y += gravity * Time.deltaTime;

        // if holding down bump up our movement amount and turn off one way platform detection for a frame.
        // this lets us jump down through one way platforms
        if (_controller.isGrounded && InputManager.GetAxisRaw("Vertical") < 0f && Mathf.Abs(_velocity.y) <= 40f && canInput == true)
        {
            _velocity.y *= 3f;
            _controller.ignoreOneWayPlatformsThisFrame = true;
        }


        _controller.move(_velocity * Time.deltaTime);

        // grab our current _velocity to use as a base for all calculations
        _velocity = _controller.velocity;
    }
    private void ClimbingState()
    {
        _animator.Play(Animator.StringToHash("Idle"));

        /*if (InputManager.GetAxisRaw("Horizontal") > 0f && canInput == true)
        {
            normalizedHorizontalSpeed = 1;
        }
        else if (InputManager.GetAxisRaw("Horizontal") < 0f && canInput == true)
        {
            normalizedHorizontalSpeed = -1;
        }
        else
        {
            normalizedHorizontalSpeed = 0;
        }*/

        if (InputManager.GetAxisRaw("Vertical") > 0f && canInput == true)
        {
            normalizedVerticalSpeed = 1;
        }
        else if (InputManager.GetAxisRaw("Vertical") < 0f && canInput == true)
        {
            normalizedVerticalSpeed = -1;
        }
        else
        {
            normalizedVerticalSpeed = 0;
        }

        _velocity.y = normalizedVerticalSpeed * climbingSpeed;
        _velocity.x = /*normalizedHorizontalSpeed * climbingSpeed * 5f*/ 0f;

        //Work in progress
        if (InputManager.GetButton("Jump") && canInput == true && _velocity.y >= 0f)
        {
            _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
            _animator.Play(Animator.StringToHash("Jump"));
        }

        _controller.move(_velocity * Time.deltaTime);

        // grab our current _velocity to use as a base for all calculations
        _velocity = _controller.velocity;

        if (_controller.isGrounded == true)
        {
            currState = State.Normal;
        }
    }
    private void CrawlingState()
    {
        if (InputManager.GetButtonDown("Crawl") && canInput == true || crawlJump == true || isFalling)
        {
            //1000000001
            LayerMask detectWhat = Convert.ToInt32("1000000001", 2);
            Vector2 endPos = new Vector2(transform.position.x, transform.position.y + thisColl.size.y/2f);
            bool colliding = Physics2D.Linecast(transform.position, endPos, detectWhat);
            crawlJump = false;
            if(!colliding)
                CrawlToNormalTransition();
            return;
        }

        if (_controller.isGrounded)
            _velocity.y = 0;

        if (InputManager.GetAxisRaw("Horizontal") > 0f && canInput == true)
        {
            normalizedHorizontalSpeed = 1;
            if (transform.localScale.x < 0f)
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

            /*if (_controller.isGrounded)
                _animator.Play(Animator.StringToHash("Run"));*/
        }
        else if (InputManager.GetAxisRaw("Horizontal") < 0f && canInput == true)
        {
            normalizedHorizontalSpeed = -1;
            if (transform.localScale.x > 0f)
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

           /* if (_controller.isGrounded)
                _animator.Play(Animator.StringToHash("Run"));*/
        }
        else
        {
            normalizedHorizontalSpeed = 0;

           /* if (_controller.isGrounded)
                _animator.Play(Animator.StringToHash("Idle"));*/
        }

        // we can only jump whilst grounded
        if (_controller.isGrounded && InputManager.GetButton("Jump") && canInput == true)
        {
            crawlJump = true;
            return;
        }

        // apply horizontal speed smoothing it. dont really do this with Lerp. Use SmoothDamp or something that provides more control
        var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
        _velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * crawlingSpeed, Time.deltaTime * smoothedMovementFactor);

        // apply gravity before moving
        _velocity.y += gravity * Time.deltaTime;

        // if holding down bump up our movement amount and turn off one way platform detection for a frame.
        // this lets us jump down through one way platforms
        if (_controller.isGrounded && InputManager.GetAxisRaw("Vertical") < 0f && canInput == true)
        {
            _velocity.y *= 3f;
            _controller.ignoreOneWayPlatformsThisFrame = true;
        }

        _controller.move(_velocity * Time.deltaTime);

        // grab our current _velocity to use as a base for all calculations
        _velocity = _controller.velocity;
    }
    private void SpecialJumpingState()
    {
        if (_controller.isGrounded)
            currState = State.Normal;

        if (InputManager.GetAxisRaw("Horizontal") > 0f && canInput == true)
        {
            normalizedHorizontalSpeed = 1;
            if (transform.localScale.x < 0f)
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        }
        else if (InputManager.GetAxisRaw("Horizontal") < 0f && canInput == true)
        {
            normalizedHorizontalSpeed = -1;
            if (transform.localScale.x > 0f)
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        }
        else
        {
            normalizedHorizontalSpeed = 0;

        }

        //falling animation
        if (_controller.isGrounded == false && _controller.velocity.y < 0.0f)
        {
            _animator.Play(Animator.StringToHash("Fall"));
        }


        // apply horizontal speed smoothing it. dont really do this with Lerp. Use SmoothDamp or something that provides more control
        var smoothedMovementFactor = inAirDamping; // how fast do we change direction?
        var currentSpeed = runSpeed;
        _velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * currentSpeed, Time.deltaTime * smoothedMovementFactor);

        // apply gravity before moving
        _velocity.y += gravity * Time.deltaTime;

        _controller.move(_velocity * Time.deltaTime);

        // grab our current _velocity to use as a base for all calculations
        _velocity = _controller.velocity;
    }
    private void KnockBackState()
    {
        _controller.move(new Vector3(50f * (-1f) * knockbackDirection, 9f, 0f));
        _velocity = _controller.velocity;

        CustomPlayerController.me.currState = State.Normal;
    }

    private void fallingCheck()
    {
        if (!_controller.isGrounded && Mathf.Round(_controller.velocity.y) < 0f)
        {
            if(markedTime == 0f) markedTime = Time.time;
            if (Time.time >= markedTime + 0.2f)
            {
                isFalling = true;
                markedTime = 0f;
            }
        }

        if (_controller.isGrounded)
        {
            isFalling = false;
            markedTime = 0f;
        }
    }
    public void CrawlToNormalTransition()
    {
        crawlJump = false;
        currState = State.Normal;
        thisColl.size = normalCollSize;
        thisColl.offset = normalCollOffset;
        _controller.totalHorizontalRays = 5;
        _controller.recalculateDistanceBetweenRays();
        return;
    }

    //getters and setters

    public State getCurrentState()
    {
        return me.currState;
    }
    public void setCurrentState(State newState)
    {
        me.currState = newState;
    }
    public bool isGrounded()
    {
        return me._controller.isGrounded;
    }
    public Vector3 getVelocity()
    {
        return _velocity;
    }
    public void setVelocity(Vector3 vel)
    {
        _velocity = vel;
    }
    public Vector2 getCollSize()
    {
        return thisColl.size;
    }
    public bool getIsFalling()
    {
        return isFalling;
    }
    public void Jump()
    {
        //_velocity.y = -_velocity.y + 0.25f * -_velocity.y;
        _velocity.y = -_velocity.y;
        currState = State.SpecialJumping;
        _animator.Play(Animator.StringToHash("Jump"));
    }
}
