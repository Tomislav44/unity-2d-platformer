﻿using UnityEngine;
using System.Collections;

public class LevelBoundScript : MonoBehaviour
{
    public static LevelBoundScript me;

    private Transform[] bounds = new Transform[3];
    private Bounds levelBounds;

    public Bounds getLevelBounds()
    {
        return levelBounds;
    }

	void Awake ()
    {
        if (!LevelBoundScript.me)
            me = this;
        else
        {
            Debug.Log("There is already an instance of this class!");
            Destroy(this);
        }
        bounds = GetComponentsInChildren<Transform>();
        levelBounds = new Bounds();
        levelBounds.SetMinMax(bounds[1].position, bounds[2].position);
    }
}
