﻿using UnityEngine;
using TeamUtility.IO;
using System.Collections;

public class PlayerRectangleFollow : MonoBehaviour
{
    public Transform player = null;
    private BoxCollider2D boxcoll2D = null;
    private BoxCollider2D playerBoxColl = null;

    private Vector3 _smoothDampVelocity;
    private bool wasHanging = false;

    public float lookOffset = 400f;
    public float lookWaitDuration = 0.8f;

    private float timeMark = 0f;
    public bool isLooking = false;
    private Vector2 previousPosition = Vector2.zero;

    public LayerMask detectWhat;
    public float fallingOffset = 0f;

    public bool specialFollow = false;

    void Awake ()
    {
        boxcoll2D = GetComponent<BoxCollider2D>();
	}
    void Start()
    {
        if (!player) player = CustomPlayerController.me.transform;
        if (!playerBoxColl) playerBoxColl = CustomPlayerController.me.GetComponent<BoxCollider2D>();

        boxcoll2D.size = new Vector2(playerBoxColl.bounds.size.x * 2.9f, playerBoxColl.bounds.size.y * 3.7f);
    }

	void Update ()
    {
        if (!lookDownAndUp()) return;

        //if (CustomPlayerController.me.isGrounded()) specialFollow = false;

        /*if (CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.SpecialJumping || specialFollow)
        {
            if (!specialFollow) specialFollow = true;
            transform.position = new Vector2(player.position.x, player.position.y);

        }*/

        if (CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.Normal ||
            CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.SpecialJumping)
        {
            //Debug.Log(specialFollow);
            //if ((!cameraFallingCheck() && player.position.y < boxcoll2D.bounds.min.y) || specialFollow)
            //if(CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.SpecialJumping || specialFollow)
            {
                //if (!specialFollow) specialFollow = true;
                //transform.position = new Vector2(player.position.x, player.position.y);
                
            }
           //else
            {
                //if (CustomPlayerController.me.isGrounded() && !boxcoll2D.OverlapPoint(player.position) && wasHanging == false)
                if (CustomPlayerController.me.isGrounded() && (!boxcoll2D.OverlapPoint(player.position) || wasHanging == true))
                {
                    //Debug.Log("Called!");
                    transform.position = new Vector2(transform.position.x, player.position.y + 500f);
                    adjustY();
                    
                }

                if
                /*(!boxcoll2D.OverlapPoint(player.position) ||
                  !boxcoll2D.OverlapPoint(playerBoxColl.bounds.max) ||
                  !boxcoll2D.OverlapPoint(playerBoxColl.bounds.min)
                )*/
                (!isWithinARange(player.position.x, boxcoll2D.bounds.min.x, boxcoll2D.bounds.max.x) ||
                !isWithinARange(playerBoxColl.bounds.max.x, boxcoll2D.bounds.min.x, boxcoll2D.bounds.max.x) ||
                !isWithinARange(playerBoxColl.bounds.min.x, boxcoll2D.bounds.min.x, boxcoll2D.bounds.max.x)
                )
                {
                    
                    adjustX();
                }

                if (CustomPlayerController.me.isGrounded()) wasHanging = false;
            }

            
        }

        if (CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.Crawling)
        {
            if
            (!isWithinARange(player.position.x, boxcoll2D.bounds.min.x, boxcoll2D.bounds.max.x) ||
                !isWithinARange(playerBoxColl.bounds.max.x, boxcoll2D.bounds.min.x, boxcoll2D.bounds.max.x) ||
                !isWithinARange(playerBoxColl.bounds.min.x, boxcoll2D.bounds.min.x, boxcoll2D.bounds.max.x)
            )
            {
                adjustX();
            }
        }

        if (CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.Climbing)
        {
            transform.position = new Vector2(player.position.x, player.position.y);
        }

        if (CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.Hanging && CustomPlayerController.me.isGrounded())
        {
            transform.position = new Vector2(player.position.x, player.position.y);
            wasHanging = true;
        }

    }

    private void adjustX()
    {
        Vector2 playerPos = player.position;
        float plWidth = playerBoxColl.bounds.size.x / 2f;

        if (playerPos.x > transform.position.x)
            transform.position = new Vector2(playerPos.x - boxcoll2D.bounds.size.x / 2f + plWidth + 5f, transform.position.y);
        else if (playerPos.x < transform.position.x)
            transform.position = new Vector2(playerPos.x + boxcoll2D.bounds.size.x / 2f - plWidth - 5f, transform.position.y);
    }
    private void adjustY()
    {
        Vector2 playerPos = player.position;
        float plHeight = playerBoxColl.bounds.size.y / 2f;

        if (playerPos.y > transform.position.y)
            transform.position = new Vector2(transform.position.x, playerPos.y - boxcoll2D.bounds.size.y / 2f + plHeight + 5f);
        else if (playerPos.y < transform.position.y)
            transform.position = new Vector2(transform.position.x, playerPos.y + boxcoll2D.bounds.size.y / 2f - plHeight - 5f);
    }
    private bool lookDownAndUp()
    {
        if ((CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.Normal ||
          CustomPlayerController.me.getCurrentState() == CustomPlayerController.State.Crawling) && CustomPlayerController.me.isGrounded()
          && InputManager.GetAxisRaw("Horizontal") == 0f)
        {

            if (InputManager.GetAxisRaw("Vertical") != 0f)
            {
                if (timeMark == 0f) timeMark = Time.time;
                if (Time.time >= timeMark + lookWaitDuration)
                {
                    if (!isLooking)
                    {
                        previousPosition = transform.position;
                        transform.position = new Vector2(transform.position.x, transform.position.y + lookOffset * InputManager.GetAxisRaw("Vertical"));
                        isLooking = true;
                    }
                }
                return false;
            }

            if (InputManager.GetAxisRaw("Vertical") == 0f && (isLooking || timeMark > 0f))
            {
                if (previousPosition != Vector2.zero) transform.position = previousPosition;
                previousPosition = Vector2.zero;
                timeMark = 0f;
                isLooking = false;
                return false;
            }
        }
        else
        {
            if (previousPosition != Vector2.zero) transform.position = previousPosition;
            previousPosition = Vector2.zero;
            timeMark = 0f;
            isLooking = false;
        }

        return true;
    }
    private bool isWithinARange(float value, float min, float max)
    {
        if (Mathf.Clamp(value, min, max) == value)
            return true;
        else
            return false;
    }

    public bool cameraFallingCheck()
    {
        Vector2 newPos = new Vector2(player.position.x, player.position.y - fallingOffset);
        Vector2 newPos2 = new Vector2(player.position.x, player.position.y + fallingOffset);
        RaycastHit2D hit = Physics2D.Linecast(player.position, newPos, detectWhat);
        RaycastHit2D hit2 = Physics2D.Linecast(player.position, newPos2, detectWhat);

        if (hit || hit2)
            return true;
        else
            return false;
    }
    void OnDrawGizmos()
    {
        Vector2 newPos = new Vector2(player.position.x, player.position.y - fallingOffset);
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(player.position, newPos);
    }
}
