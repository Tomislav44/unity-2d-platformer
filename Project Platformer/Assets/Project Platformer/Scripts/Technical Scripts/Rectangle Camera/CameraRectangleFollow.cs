﻿using UnityEngine;
using System.Collections;

public class CameraRectangleFollow : MonoBehaviour
{
    public Transform target;
    public float smoothDampTime = 0f;
    [HideInInspector]
    public new Transform transform;
    public Vector3 cameraOffset;
    public bool useFixedUpdate = false;

    private Vector3 _smoothDampVelocity = Vector3.zero;
    private Camera cam;

    private float CameraVelocity = 0f;
    private float TargetVelocity = 0f;
    private Vector3 previous;
    private Vector3 previousTarget;

    private Vector2 bottomLeftPoint, upperRightPoint;

    private float defaultDampTime = 0f;
    private PlayerRectangleFollow rec;

    void Awake()
    {
        rec = target.GetComponent<PlayerRectangleFollow>();
        transform = gameObject.transform;
        defaultDampTime = smoothDampTime;
        cam = GetComponent<Camera>();
    }
    void Update()
    {
        bottomLeftPoint = cam.ScreenToWorldPoint(new Vector3(0, 0, cam.nearClipPlane));
        upperRightPoint = cam.ScreenToWorldPoint(new Vector3(cam.pixelWidth, cam.pixelHeight, cam.nearClipPlane));

        //Debug.Log(LevelBoundScript.me.getLevelBounds().Contains(bottomLeftPoint));
    }
    void LateUpdate()
    {
        if (!useFixedUpdate)
            updateCameraPosition();
    }
    void FixedUpdate()
    {
        if (useFixedUpdate)
            updateCameraPosition();
    }


    void updateCameraPosition()
    {
        /*
                CameraVelocity = ((transform.position - previous).magnitude) / Time.deltaTime;
        previous = transform.position;
        if (Mathf.Round(CustomPlayerController.me._velocity.magnitude) != 0)
            CameraVelocity = Mathf.Infinity;

        if (CameraVelocity > 35f || Vector2.Distance(transform.position, target.position) > 30f)
            transform.position = Vector3.SmoothDamp(transform.position, target.position - cameraOffset, ref _smoothDampVelocity, smoothDampTime);
        */

        if (rec.specialFollow)
        {
            //smoothDampTime = 0.01f;
        }
        else
        {
            smoothDampTime = defaultDampTime;
        }

        Vector3 min = LevelBoundScript.me.getLevelBounds().min;
        Vector3 max = LevelBoundScript.me.getLevelBounds().max;

        CameraVelocity = ((transform.position - previous).magnitude) / Time.fixedDeltaTime;
        TargetVelocity = ((target.position - previousTarget).magnitude) / Time.fixedDeltaTime;

        previous = transform.position;
        previousTarget = target.position;

        //Debug.Log(Vector2.Distance(transform.position, target.position));

        if (Mathf.Round(TargetVelocity) != 0 || (target.GetComponent<PlayerRectangleFollow>().isLooking) || Vector2.Distance(transform.position, target.position) > 50f)
            CameraVelocity = Mathf.Infinity;

       // Debug.Log(CameraVelocity);
        if (CameraVelocity > 50f)
        {
            transform.position = Vector3.SmoothDamp(transform.position, target.position - cameraOffset, ref _smoothDampVelocity, smoothDampTime);
        }
        
        Vector2 pnt1 = new Vector2((min.x + (upperRightPoint.x - bottomLeftPoint.x) + min.x) / 2, (min.y + (upperRightPoint.y - bottomLeftPoint.y) + min.y) / 2);
        Vector2 pnt2 = new Vector2((max.x + max.x - (upperRightPoint.x - bottomLeftPoint.x)) / 2, (max.y + max.y - (upperRightPoint.y - bottomLeftPoint.y)) / 2);

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, pnt1.x, pnt2.x), Mathf.Clamp(transform.position.y, pnt1.y, pnt2.y), transform.position.z);
        //CameraBoundScript.me.Adjustment();
    }
}
