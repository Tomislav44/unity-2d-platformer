﻿using UnityEngine;
using System.Collections;

public class CameraBoundScript : MonoBehaviour
{
    public static CameraBoundScript me;

    private Camera cam;

    private Vector3 bottom_left;
    private Vector3 upper_right;

    private Bounds scrBounds = new Bounds();

    public Bounds screenBounds
    {
        get { return scrBounds; }
        set { scrBounds = value; }
    }

    void Awake()
    {
        if (!CameraBoundScript.me)
            me = this;
        else
        {
            Debug.Log("There is already an instance of this class!");
            Destroy(this);
        }

        cam = GetComponent<Camera>();
        Adjustment();
    }

    public void Adjustment()
    {
        bottom_left = cam.ViewportToWorldPoint(new Vector2(0, 0));
        upper_right = cam.ViewportToWorldPoint(new Vector2(1, 1));

        scrBounds.SetMinMax(bottom_left, upper_right);

        Vector3 newCenter = scrBounds.center;
        newCenter.z = 0f;

        scrBounds.center = newCenter;
    }

    void Update()
    {
        //Debug.Log(scrBounds);
        Adjustment();
    }
}

