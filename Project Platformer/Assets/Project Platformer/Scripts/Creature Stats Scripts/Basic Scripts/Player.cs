﻿using UnityEngine;
using System.Collections;
using System;

public class Player : Creature
{
    public enum PlayerState
    {
        Normal,
        Knockback
    }

    private PlayerState curr_state;

    public PlayerState current_state
    {
        get { return curr_state; }
        set
        {
            curr_state = value;

            if (value == PlayerState.Knockback)
            {
                knock_start = Time.time;
                knock_effect_start = Time.time;
            }
        }
    }

    private float knock_start = 0f; //time when invincibility started
    private float knock_duration = 1.2f; //invincibility duration in seconds

    private float knock_effect_start = 0f;
    private float knock_effect_dur = 0.1f;

    void Awake()
    {
        Health = 100f;
    }

    void Update()
    {
        //Debug.Log(current_state);

        if (current_state == PlayerState.Knockback)
        {
            SpriteRenderer rend = GetComponent<SpriteRenderer>();

            if (Time.time >= knock_start + knock_duration)
            {
                rend.color = new Color(rend.color.r, rend.color.b, rend.color.g, 1f);
                current_state = PlayerState.Normal;
                return;
            }

            if (Time.time >= knock_effect_start + knock_effect_dur)
            {
                rend.color = new Color(rend.color.r, rend.color.b, rend.color.g, rend.color.a >= 0.5f ? 0f : 1f);
                knock_effect_start = Time.time;
            }
        }
    }

    protected override void onDeath()
    {
        //Debug.Log("Player died!");
    }

    protected override void onHit(HitInformation hit)
    {
        if (current_state == PlayerState.Normal)
        {
            CustomPlayerController.me.setCurrentState(CustomPlayerController.State.Knockback);
            CustomPlayerController.me.knockbackDirection = hit.hit_direction;
            Health -= hit.hit_damage;
            current_state = PlayerState.Knockback;
            Debug.Log("Player Hit!");
        }
    }

}
