﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Enemy : Creature
{
    protected void Update() { Behaviour(); }
    protected abstract void Behaviour();
}
