﻿using UnityEngine;
using System.Collections;

public abstract class Creature : MonoBehaviour
{
    private float health;

    public float Health
    {
        get { return health; }
        set { health = value;  if (health <= 0f) onDeath(); }
    }
    protected abstract void onDeath();
    protected abstract void onHit(HitInformation hit);
}
