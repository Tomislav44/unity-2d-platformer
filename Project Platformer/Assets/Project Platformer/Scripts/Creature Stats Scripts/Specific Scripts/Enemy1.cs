﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy1 : Enemy
{
    public float speed = 1f;
    public float gravity = -2000f;

    public Transform sightStart;
    public Transform sightEnd;

    public LayerMask detectWhat;

    public bool colliding;
    public float rightPointOffset = 0f;

    public Transform DeathParticle;

    private Vector2 rightPoint;
    private Vector2 secondRightPoint;

    private Animator anim;
    private Rigidbody2D rb2d;
    private BoxCollider2D _boxCollider2D;

    private SpriteRenderer rend;
    private float mark = 0f;

    private HitInformation hit;

    void Awake()
    {
        Health = 100f;
        rend = GetComponent<SpriteRenderer>();

        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        _boxCollider2D = GetComponent<BoxCollider2D>();


        if (transform.localScale.x == -1f)
        {
            speed *= -1f;
        }

        Physics2D.queriesStartInColliders = true;

        hit = ScriptableObject.CreateInstance("HitInformation") as HitInformation;
        hit.hit_damage = 50f;
    }
    protected override void onDeath()
    {
        speed = 0;
        Instantiate(DeathParticle, transform.position, transform.rotation);
        anim.enabled = false;
        GetComponent<EnemyDeathFade>().enabled = true;
        enabled = false;
        Destroy(rb2d);
        //Destroy(this.gameObject);
    }
    protected override void onHit(HitInformation hit)
    {
        Health -= hit.hit_damage;
        rend.color = Color.HSVToRGB(269,173,219);
        mark = Time.time;
    }
    protected override void Behaviour()
    {
        if (Time.time >= mark + 0.2f)
            rend.color = Color.white;

        rb2d.velocity = new Vector2(speed, rb2d.velocity.y + gravity * Time.smoothDeltaTime);
        colliding = Physics2D.Linecast(sightStart.position, sightEnd.position, detectWhat);

        if (transform.localScale.x == 1f)
        {
            rightPoint = new Vector2(_boxCollider2D.bounds.max.x, _boxCollider2D.bounds.min.y);
            secondRightPoint = new Vector2(rightPoint.x, rightPoint.y - rightPointOffset);
        }
        else if (transform.localScale.x == -1f)
        {
            rightPoint = new Vector2(_boxCollider2D.bounds.min.x, _boxCollider2D.bounds.min.y);
            secondRightPoint = new Vector2(rightPoint.x, rightPoint.y - rightPointOffset);
        }

        bool edgeTest = Physics2D.Linecast(rightPoint, secondRightPoint, detectWhat);

        if (colliding || !edgeTest)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            speed *= -1;
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player"
            &&
    ((other.transform.position.y - CustomPlayerController.me.getCollSize().y / 2f >= _boxCollider2D.bounds.center.y - _boxCollider2D.bounds.extents.y / 2.0f
    && (Mathf.Round(CustomPlayerController.me._velocity.y) < 0f))
            || (Mathf.Round(CustomPlayerController.me._velocity.y) < 0f)))
        {
            CustomPlayerController.me.Jump();
            //CustomPlayerController.me.setVelocity(new Vector3(0f, 0f, 0f));
            Debug.Log("Dies!");
            //onDeath();
        }
        else if (other.CompareTag("Player"))
        {
            //Debug.Log("Player " + other.transform + " Hit!");
            hit.hit_direction = transform.position.x > other.transform.position.x ? 1 : -1;
            other.SendMessage("onHit", hit);
        }
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(sightStart.position, sightEnd.position);

        Gizmos.color = Color.green;
        Gizmos.DrawLine(rightPoint, secondRightPoint);
    }

}
