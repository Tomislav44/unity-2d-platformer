﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy2 : Enemy
{
    public float speed = 1f;
    public float gravity = -2000f;

    public Transform sightStart;
    public Transform sightEnd;

    public LayerMask detectWhat;

    public bool colliding;

    private Animator anim;
    private Rigidbody2D rb2d;
    private BoxCollider2D _boxCollider2D;
    private Color defColor;

    private SpriteRenderer rend;
    private float mark = 0f;

    private int jumpingMark = 0; //frames

    private HitInformation hit;

    void Awake()
    {
        Health = 5000f;
        rend = GetComponent<SpriteRenderer>();

        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        _boxCollider2D = GetComponent<BoxCollider2D>();

        if (transform.localScale.x == -1f)
        {
            speed *= -1f;
        }

        Physics2D.queriesStartInColliders = true;
        defColor = rend.color;

        hit = ScriptableObject.CreateInstance("HitInformation") as HitInformation;
        hit.hit_damage = 50f;
    }
    protected override void onDeath()
    {
        speed = 0f;
        rb2d.velocity = new Vector2(0f, rb2d.velocity.y + gravity * Time.deltaTime);
        anim.Play(Animator.StringToHash("EnemyStomped"));
        Destroy(this.gameObject, 0.5f);
    }
    protected override void onHit(HitInformation hit)
    {
        Health -= hit.hit_damage;
        rend.color = Color.HSVToRGB(269, 173, 219);
        mark = Time.time;
    }
    protected override void Behaviour()
    {
        if (speed != 0f)
        {
            if (Time.time >= mark + 0.2f)
                rend.color = defColor;

            if (jumpingMark >= 150)
            {
                //Debug.Log("Jumping!");
                jumpingMark = 0;
                rb2d.velocity = new Vector2(speed, 800f);
            }

            rb2d.velocity = new Vector2(speed, rb2d.velocity.y + gravity * Time.deltaTime);
            colliding = Physics2D.Linecast(sightStart.position, sightEnd.position, detectWhat);

            if (colliding)
            {
                transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
                speed *= -1;
            }

            jumpingMark++;
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player"
            &&
    ((other.transform.position.y - CustomPlayerController.me.getCollSize().y / 2f >= _boxCollider2D.bounds.center.y - _boxCollider2D.bounds.extents.y / 2.0f
    && (Mathf.Round(CustomPlayerController.me._velocity.y) < 0f))
            || (Mathf.Round(CustomPlayerController.me._velocity.y) < 0f)))
        {
            CustomPlayerController.me.Jump();
            //CustomPlayerController.me.setVelocity(new Vector3(0f, 0f, 0f));
            Debug.Log("Dies!");
            //onDeath();
        }
        else if (other.CompareTag("Player"))
        {
            //Debug.Log("Player " + other.transform + " Hit!");
            hit.hit_direction = transform.position.x > other.transform.position.x ? 1 : -1;
            other.SendMessage("onHit", hit);
        }
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(sightStart.position, sightEnd.position);
    }

}
