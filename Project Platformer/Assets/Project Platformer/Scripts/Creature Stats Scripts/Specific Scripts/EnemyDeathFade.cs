﻿using UnityEngine;
using System.Collections;

public class EnemyDeathFade : MonoBehaviour
{
    public float minimum = 0.0f;
    public float maximum = 1f;
    public float duration = 1.0f;
    private float startTime;
    public SpriteRenderer sprite;

    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        startTime = Time.time;
    }
    void Update()
    {
        float t = (Time.time - startTime) / duration;
        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, Mathf.SmoothStep(maximum, minimum, t));

        if (sprite.color.a <= 0f) Destroy(gameObject);
    }
}
