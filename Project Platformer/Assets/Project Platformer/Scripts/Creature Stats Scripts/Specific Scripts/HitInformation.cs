﻿using UnityEngine;
using System.Collections;

public class HitInformation :  ScriptableObject
{
    private float hitdmg;
    private int hitdir = 1;

    public float hit_damage
    {
        get { return hitdmg; }
        set { hitdmg = value; }
    }
        
    public int hit_direction
    {
        get { return hitdir; }
        set { hitdir = value; }
    }

    public HitInformation(float hit_damage, int hit_direction = 1)
    {
        this.hit_damage = hit_damage;
        this.hit_direction = hit_direction;
    }
}
