﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy4 : Enemy
{
    public float speed = 1f;
    public float gravity = -2000f;

    public Transform sightStart;
    public Transform sightEnd;

    public Transform detectStart;
    public Transform detectEnd;

    public LayerMask detectWhat, detectWhat2;

    public bool colliding;

    private Animator anim;
    private Rigidbody2D rb2d;
    private BoxCollider2D _boxCollider2D;

    private SpriteRenderer rend;
    private float mark = 0f;

    enum Enemy4State{Normal, Triggered}
    Enemy4State currentState = Enemy4State.Normal;

    public Sprite currentSprite;
    public Sprite newSprite;

    private float specialMark = 0f;

    void Awake()
    {
        Health = 100f;
        rend = GetComponent<SpriteRenderer>();

        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        _boxCollider2D = GetComponent<BoxCollider2D>();


        if (transform.localScale.x == -1f)
        {
            speed *= -1f;
        }

        Physics2D.queriesStartInColliders = true;
    }
    protected override void onDeath()
    {
        speed = 0;
        anim.Play(Animator.StringToHash("EnemyStomped"));
        Destroy(this.gameObject, 0.5f);
    }
    protected override void onHit(HitInformation hit)
    {
        Health -= hit.hit_damage;
        rend.color = Color.HSVToRGB(269, 173, 219);
        mark = Time.time;
    }
    protected override void Behaviour()
    {
        if (Time.time >= mark + 0.2f)
            rend.color = Color.white;

        rb2d.velocity = new Vector2(speed, rb2d.velocity.y + gravity * Time.deltaTime);
        colliding = Physics2D.Linecast(sightStart.position, sightEnd.position, detectWhat);

        if (colliding)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            speed *= -1;
        }

        RaycastHit2D hit = Physics2D.Linecast(detectStart.position, detectEnd.position, detectWhat2);

        //Debug.Log(hit.transform);

        if (hit && hit.transform.CompareTag("Player") && currentState == Enemy4State.Normal && CustomPlayerController.me.isGrounded() == false)
        {
            NormalToTriggered();
        }

        bool test = false;

        if (hit && hit.transform.CompareTag("Player"))
        {
            test = true;
        }

        if (!test && specialMark >= 30 && currentState == Enemy4State.Triggered)
        {
            TriggeredToNormal();
        }

        specialMark++;
    }
    void NormalToTriggered()
    {
        currentState = Enemy4State.Triggered;
        rend.sprite = newSprite;
        _boxCollider2D.size = new Vector2(_boxCollider2D.size.x, 60f);
        transform.position = new Vector2(transform.position.x, transform.position.y + 10f);
        specialMark = 0;
    }
    void TriggeredToNormal()
    {
        currentState = Enemy4State.Normal;
        rend.sprite = currentSprite;
        _boxCollider2D.size = new Vector2(_boxCollider2D.size.x, 40f);
        transform.position = new Vector2(transform.position.x, transform.position.y - 10f);
    }
    void OnTriggerEnter2D(Collider2D other)
    {


        if (other.gameObject.tag == "Player"
            &&
    ((other.transform.position.y - CustomPlayerController.me.getCollSize().y / 2f >= _boxCollider2D.bounds.center.y - _boxCollider2D.bounds.extents.y / 2.0f
    && (Mathf.Round(CustomPlayerController.me._velocity.y) < 0f))
            || (Mathf.Round(CustomPlayerController.me._velocity.y) < 0f)))
        {
            CustomPlayerController.me.setVelocity(new Vector3(0f, 0f, 0f));
            //Debug.Log("Spiked!");
        }
        else if (other.CompareTag("Player"))
        {
            //Debug.Log("Player " + other.transform + " Hit!");
            other.SendMessage("onHit", 50f);
        }
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(sightStart.position, sightEnd.position);

        Gizmos.color = Color.green;
        Gizmos.DrawLine(detectStart.position, detectEnd.position);
    }

}
