﻿using UnityEngine;
using System.Collections;

public abstract class Weapon : MonoBehaviour
{
    public float str = 0f;

    abstract protected void onFire();

}
