﻿using UnityEngine;
using System.Collections;

public class Sword1 : Weapon
{
    public Transform bottom = null;

    private Collider2D col2D = null;
    private float fireTime = 0f;

    void Awake()
    {
        str = 5f;
        col2D = GetComponent<BoxCollider2D>();
    }
    protected override void onFire()
    {
        col2D.enabled = true;
        fireTime = Time.time;

        //rayMode();
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            Debug.Log("Hit!");
        }
    }

    void Update()
    {
        if (Time.time > fireTime + 0.2f)
        {
            col2D.enabled = false;
        }

    }

    void rayMode()
    {
        RaycastHit2D hit;
        Vector2 direction;

        if (transform.parent.transform.localScale.x >= 0f)
            direction = Vector2.right;
        else
            direction = Vector2.left;

        hit = Physics2D.Raycast(bottom.position, direction, 1.8f, 1 << LayerMask.NameToLayer("Interactables"));

        if (hit.transform.CompareTag("Enemy"))
        {
            Debug.Log("Hit!");
        }

        col2D.enabled = false;
    }
}
