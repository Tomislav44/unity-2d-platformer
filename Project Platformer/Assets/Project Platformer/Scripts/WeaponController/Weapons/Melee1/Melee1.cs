﻿using UnityEngine;
using System.Collections;

public class Melee1 : Weapon
{
    public Transform projectile;
    public float difference = 0f;
    public float fireRate;
    private Collider2D col2D = null;
    public Transform parentTransform;

    private float nextFire;

    void Awake()
    {
        str = 5f;
        col2D = projectile.GetComponent<BoxCollider2D>();
        parentTransform = transform.parent;
    }
    protected override void onFire()
    {
        col2D.enabled = true;

        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;

            Transform temp = Instantiate<Transform>(projectile);

            temp.position = transform.position;
            temp.rotation = transform.rotation;
            temp.localScale = parentTransform.localScale;
            
            temp.Translate(new Vector3(50f * parentTransform.localScale.x, difference, 0f));

            temp.SetParent(transform);
        }
    }

    void Update()
    {
        /*if (Time.time > fireTime + 0.2f)
        {
            col2D.enabled = false;
        }*/
    }
}
