﻿using UnityEngine;
using System.Collections;

public class MeleeProjectile : MonoBehaviour
{
    public float speed;
    public float distance = 0f;
    public float direction = 1f;
    public float damage = 0f;

    private Vector2 startingPos;

    void Start()
    {
        startingPos = transform.localPosition;
        direction = transform.parent.parent.localScale.x;
    }

    void Update()
    {
        transform.Translate(Vector3.right * direction * speed * Time.deltaTime);
        if (Vector2.Distance(transform.localPosition, startingPos) >= distance || transform.parent.parent.localScale.x != direction)
            Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            Debug.Log("Enemy " + other.transform + " Hit!");
            other.SendMessage("onHit", damage);
            Destroy(gameObject);
        }
    }
}
