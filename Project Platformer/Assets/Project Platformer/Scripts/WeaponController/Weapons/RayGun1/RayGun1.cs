﻿using UnityEngine;
using TeamUtility.IO;
using System.Collections;

public class RayGun1 : Weapon
{
    public Transform projectile;
    public float difference = 0f;
    public float fireRate;
    public Transform parentTransform;

    public Transform targetTransform;
    public float speed;

    private float nextFire;

    private bool points = true;

    void Awake()
    {
        str = 5f;
        parentTransform = transform.parent;
    }
    void Update()
    {
        if(WeaponController.targetTransform) targetTransform = WeaponController.targetTransform;
        points = true;
        Vector3 vectorToTarget;

        if (targetTransform) vectorToTarget = targetTransform.position - transform.position;
        else vectorToTarget = Vector3.zero;

        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;

        if (parentTransform.localScale.x == -1f)
        {
            angle = 180f - angle;
            if (angle >= 270f && angle <= 360f) angle = angle - 360f;
        }

        angle = Mathf.Clamp(angle, -90f, 90f);

        if (targetTransform)
        {
            if (parentTransform.localScale.x == 1f && (targetTransform.position.x < transform.position.x) ||
            parentTransform.localScale.x == -1f && (targetTransform.position.x > transform.position.x))
            {
                angle = 0f;
                points = false;
            }
        }

        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        if(targetTransform) transform.rotation = q;
    }
    protected override void onFire()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;

            Transform temp = Instantiate<Transform>(projectile);

            temp.position = transform.position;
            temp.rotation = transform.rotation;
            temp.localScale = parentTransform.localScale;

            if(points) temp.GetComponent<RayProjectile>().target = targetTransform;

            //EditorApplication.isPaused = true;
        }
    }
}
