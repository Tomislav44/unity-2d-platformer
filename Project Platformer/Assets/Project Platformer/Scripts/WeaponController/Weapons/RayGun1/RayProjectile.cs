﻿using UnityEngine;
using System.Collections;

public class RayProjectile : MonoBehaviour
{
    public float speed;
    public float damage;
    public float direction = 1f;

    public LayerMask detectWhat;

    public Transform sightStart;
    public Transform sightEnd;

    public Transform target;

    private Rigidbody2D myRigidbody;

    private HitInformation hit;

    private Vector2 currentNormal;
    private int frameCounter = 0, frameCounter2 = 0;

    //Bug - when using on slopes with small weapon angle, maybe fixed
    //Works bad if many projectiles
    void Start()
    {
        direction = transform.localScale.x;

        transform.localScale = new Vector3(1f, 1f, 1f);

        //myRigidbody = GetComponent<Rigidbody2D>();

        Quaternion rot = transform.rotation;

        if (direction == 1f)
            rot.eulerAngles = new Vector3(rot.eulerAngles.x, rot.eulerAngles.y, rot.eulerAngles.z);
        else if (direction == -1f)
            rot.eulerAngles = new Vector3(rot.eulerAngles.x, rot.eulerAngles.y, 180f - rot.eulerAngles.z);

        transform.rotation = rot;

        hit = ScriptableObject.CreateInstance("HitInformation") as HitInformation;
        hit.hit_damage = damage;
    }

    void Update()
    {
        RaycastHit2D hit = Physics2D.Linecast(sightStart.position, sightEnd.position, detectWhat);
        if (hit.transform != null)
        {
            if((hit.normal.x == 0f || hit.normal.x == 1f || hit.normal.x == -1f) && (hit.normal.y == 0f || hit.normal.y == 1f || hit.normal.y == -1f))
                    currentNormal = hit.normal;
        }

        //Debug.Log("hit trans " + hit.transform);
        //Debug.Log("currNorm " + currentNormal);

        //transform.Translate(Vector3.right * speed * Time.smoothDeltaTime);

        if (target)
        {
            Vector3 vectorToTarget = target.position - transform.position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = q;

            transform.position = Vector3.MoveTowards(transform.position, target.position, Time.smoothDeltaTime * speed);
        }
        else
        {
            transform.Translate(Vector3.right * speed * Time.smoothDeltaTime);
        }

        frameCounter2++;

    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(sightStart.position, sightEnd.position);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            //Debug.Log("Enemy " + other.transform + " Hit!");

            other.SendMessage("onHit", hit);
            Destroy(gameObject);
        }

        if (other.gameObject.layer == LayerMask.NameToLayer("Default"))
        {
            Destroy(gameObject);
        }

        /*if (other.gameObject.layer == (LayerMask.NameToLayer("Default")) && frameCounter2 >= 5)
        {
            frameCounter = 0;
            Vector2 inDirection = transform.right.normalized;
            Vector2 inNormal = currentNormal.normalized;

            Vector2 outDirection = Vector2.Reflect(inDirection, inNormal);

            //Debug.Log("inDir " + inDirection);
            //Debug.Log("outDir " + outDirection);

            transform.right = outDirection;

            //EditorApplication.isPaused = true;
        }*/
    }

    void OnTriggerStay2D(Collider2D other)
    {
        /*if (other.gameObject.layer == (LayerMask.NameToLayer("Default")) && frameCounter2 >= 5)
        {
            frameCounter++;
            //Debug.Log(frameCounter);
        }

        if (frameCounter >= 4)
        {
            frameCounter = 0;
            Vector2 inDirection = transform.right.normalized;
            Vector2 inNormal = currentNormal.normalized;

            Vector2 outDirection = Vector2.Reflect(inDirection, inNormal);

            //Debug.Log("inDir " + inDirection);
            //Debug.Log("outDir " + outDirection);

            transform.right = outDirection;
            frameCounter2 = 0;
        }*/
    }
}
