﻿using UnityEngine;
using TeamUtility.IO;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class WeaponController : MonoBehaviour
{
    public Weapon currentWeapon = null;
    public static Transform targetTransform = null;
    public GameObject arrow = null;
    public GameObject instance = null;

    public LayerMask detectWhat;

    private bool targetSelect = false;
    private bool initialized = false;

    private List<Collider2D> enemies = new List<Collider2D>();

    private int selection = 0;

    //private Animator _animator = null;

    void Awake()
    {
        //_animator = currentWeapon.GetComponent<Animator>();
    }
	
	void Update ()
    {
        if (InputManager.GetButton("Fire"))
        {
            //_animator.Play(Animator.StringToHash("Attack"));
            currentWeapon.SendMessage("onFire");
        }

        if (targetTransform == null)
        {
            Vector2 top_left = Vector2.zero;
            Vector2 bottom_right = Vector2.zero;

            top_left.x = CameraBoundScript.me.screenBounds.min.x;
            top_left.y = CameraBoundScript.me.screenBounds.max.y;

            bottom_right.x = CameraBoundScript.me.screenBounds.max.x;
            bottom_right.y = CameraBoundScript.me.screenBounds.min.y;

            enemies = null;
            enemies = Physics2D.OverlapAreaAll(top_left, bottom_right, detectWhat).ToList<Collider2D>();

            Collider2D min_dist = null;

            if (enemies.Count > 0)
            {
                min_dist = enemies[0];

                foreach (Collider2D item in enemies)
                {
                    if (Vector2.Distance(transform.position, item.transform.position) < Vector2.Distance(transform.position, min_dist.transform.position))
                    {
                        min_dist = item;
                    }
                }

                instance = Instantiate(arrow) as GameObject;

                instance.transform.parent = min_dist.transform;
                instance.transform.position = min_dist.transform.position;
                instance.transform.localScale = min_dist.transform.localScale;

                instance.transform.Translate(new Vector3(0f, 40f, 0f));

                targetTransform = min_dist.transform;
            }
        }

        if (InputManager.GetButtonDown("TargetSelect") && !targetSelect)
        {
            Time.timeScale = 0.0f;
            targetSelect = true;
            //Destroy(instance);
        }
        else if (InputManager.GetButtonDown("TargetSelect") && targetSelect)
        {
            targetTransform = enemies[selection].transform;

            Time.timeScale = 1.0f;
            targetSelect =  false;
            initialized = false;
            selection = 0;
            enemies = null;
        }

        if (targetSelect)
        {
            if (!initialized)
            {
                targetSelectionInitialize();
                return;
            }

            if (InputManager.GetButtonDown("MenuLeft") || InputManager.GetButtonDown("MenuRight"))
            {
                if(InputManager.GetButtonDown("MenuRight"))
                    selection += 1;
                else if (InputManager.GetButtonDown("MenuLeft"))
                    selection -= 1;

                if (enemies.Count != 0)
                {
                    if (selection == -1) selection = enemies.Count - 1;

                    selection = selection % enemies.Count;

                    Destroy(instance);
                    instance = Instantiate(arrow) as GameObject;

                    instance.transform.parent = enemies[selection].transform;
                    instance.transform.position = enemies[selection].transform.position;
                    instance.transform.localScale = enemies[selection].transform.localScale;

                    instance.transform.Translate(new Vector3(0f, 40f, 0f));
                }
            }
        }
    }

    void targetSelectionInitialize()
    {
        Vector2 top_left = Vector2.zero;
        Vector2 bottom_right = Vector2.zero;

        top_left.x = CameraBoundScript.me.screenBounds.min.x;
        top_left.y = CameraBoundScript.me.screenBounds.max.y;

        bottom_right.x = CameraBoundScript.me.screenBounds.max.x;
        bottom_right.y = CameraBoundScript.me.screenBounds.min.y;

        enemies = null;
        enemies = Physics2D.OverlapAreaAll(top_left, bottom_right, detectWhat).ToList<Collider2D>();

        initialized = true;

        if (instance) selection = enemies.IndexOf(instance.transform.parent.GetComponent<Collider2D>());
        else if (!instance) instance = Instantiate(arrow) as GameObject;

        instance.transform.parent = enemies[selection].transform;
        instance.transform.position = enemies[selection].transform.position;
        instance.transform.localScale = enemies[selection].transform.localScale;

        instance.transform.Translate(new Vector3(0f, 40f, 0f));
    }
}

