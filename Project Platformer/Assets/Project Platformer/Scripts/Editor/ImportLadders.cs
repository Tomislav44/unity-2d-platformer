﻿using UnityEngine;
using Tiled2Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[Tiled2Unity.CustomTiledImporter]
class CustomImporterAddComponent : Tiled2Unity.ICustomTiledImporter
{
    public void HandleCustomProperties(UnityEngine.GameObject gameObject, IDictionary<string, string> props)
    {}

    public void CustomizePrefab(GameObject prefab)
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Ladders");

        for (int i = 0; i < objects.Length; ++i)
        {
            if(objects[i].GetComponentInChildren<PolygonCollider2D>().gameObject.GetComponent<Ladders>() == null)
                objects[i].GetComponentInChildren<PolygonCollider2D>().gameObject.AddComponent<Ladders>();
            objects[i].GetComponentInChildren<PolygonCollider2D>().isTrigger = true;
            objects[i].GetComponentInChildren<SortingLayerExposed>();
        }

        Debug.Log("Ladders imported successfully!");
    }
}
