﻿using UnityEngine;
using Tiled2Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System;

[Tiled2Unity.CustomTiledImporter]
public class ImportInteraction : Tiled2Unity.ICustomTiledImporter
{

    public void HandleCustomProperties(UnityEngine.GameObject gameObject, IDictionary<string, string> props)
    { }

    public void CustomizePrefab(GameObject prefab)
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Interaction");

        for (int i = 0; i < objects.Length; ++i)
        {

            if (objects[i].GetComponentInChildren<PolygonCollider2D>() && objects[i].GetComponentInChildren<PolygonCollider2D>().gameObject.GetComponent(objects[i].name) == null)
            {
                //Debug.Log(objects[i].name + ",Assembly-CSharp");
                //Debug.Log(Type.GetType(objects[i].name + ",Assembly-CSharp"));
                objects[i].GetComponentInChildren<PolygonCollider2D>().gameObject.AddComponent(Type.GetType(objects[i].name + ",Assembly-CSharp", false, false));
            }
            if(objects[i].GetComponentInChildren<PolygonCollider2D>()) objects[i].GetComponentInChildren<PolygonCollider2D>().isTrigger = true;
        }

        Debug.Log("Interaction imported successfully!");
    }
}
