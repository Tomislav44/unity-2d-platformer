﻿using UnityEngine;
using Tiled2Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[Tiled2Unity.CustomTiledImporter]
class ImportEdges : Tiled2Unity.ICustomTiledImporter
{
    public void HandleCustomProperties(UnityEngine.GameObject gameObject, IDictionary<string, string> props)
    { }

    public void CustomizePrefab(GameObject prefab)
    {
        BoxCollider2D[] edges = prefab.GetComponentsInChildren<BoxCollider2D>();

        for (int i = 0; i < edges.Length; ++i)
        {
            if (edges[i].gameObject.name == "Left")
            {

                edges[i].transform.position = new Vector3(edges[i].transform.position.x + edges[i].offset.x, edges[i].transform.position.y + edges[i].offset.y, 0f);
                edges[i].offset = new Vector2(0f, 0f);
                //edges[i].enabled = false;

                string prefabPath = "Assets/Project Platformer/Prefabs/LeftEdge.prefab";
                UnityEngine.Object spawn = AssetDatabase.LoadAssetAtPath(prefabPath, typeof(GameObject));
                if (spawn != null)
                {
                    Transform oldTileObject = prefab.transform.Find("LeftEdge");
                    if (oldTileObject != null)
                    {
                        GameObject.DestroyImmediate(oldTileObject.gameObject);
                    }

                    GameObject spawnInstance =
                        (GameObject)GameObject.Instantiate(spawn);
                    spawnInstance.name = spawn.name;

                    spawnInstance.transform.parent = edges[i].gameObject.transform;
                    spawnInstance.transform.localPosition = Vector3.zero;

                    spawnInstance.GetComponentInChildren<EdgeCrawlTransition>().parentPlatform = edges[i].transform;
                    spawnInstance.GetComponentInChildren<EdgeCrawlTransition>().parentBox = edges[i].gameObject.GetComponent<BoxCollider2D>();
                }
            }

            if (edges[i].gameObject.name == "Right")
            {

                edges[i].transform.position = new Vector3(edges[i].transform.position.x + edges[i].offset.x, edges[i].transform.position.y + edges[i].offset.y, 0f);
                edges[i].offset = new Vector2(0f, 0f);
                //edges[i].enabled = false;

                string prefabPath = "Assets/Project Platformer/Prefabs/RightEdge.prefab";
                UnityEngine.Object spawn = AssetDatabase.LoadAssetAtPath(prefabPath, typeof(GameObject));
                if (spawn != null)
                {
                    Transform oldTileObject = prefab.transform.Find("RightEdge");
                    if (oldTileObject != null)
                    {
                        GameObject.DestroyImmediate(oldTileObject.gameObject);
                    }

                    GameObject spawnInstance =
                        (GameObject)GameObject.Instantiate(spawn);
                    spawnInstance.name = spawn.name;

                    spawnInstance.transform.parent = edges[i].gameObject.transform;
                    spawnInstance.transform.localPosition = Vector3.zero;

                    spawnInstance.GetComponentInChildren<EdgeCrawlTransition>().parentPlatform = edges[i].transform;
                    spawnInstance.GetComponentInChildren<EdgeCrawlTransition>().parentBox = edges[i].gameObject.GetComponent<BoxCollider2D>();
                }
            }
            Component.DestroyImmediate(edges[i]);
        }

        Debug.Log("Edges imported successfully!");
    }
}
