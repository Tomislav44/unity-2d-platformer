﻿using UnityEngine;
using Tiled2Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[Tiled2Unity.CustomTiledImporter]
class ImportPrefabInteraction : Tiled2Unity.ICustomTiledImporter
{
    int prefabNum = 1;

    Dictionary<GameObject, IDictionary<string, string>> dics = new Dictionary<GameObject, IDictionary<string, string>>();

    public void HandleCustomProperties(UnityEngine.GameObject gameObject, IDictionary<string, string> props)
    {
        if(gameObject != null && props != null) dics.Add(gameObject, props);
    }

    public void CustomizePrefab(GameObject prefab)
    {
        Transform InteractionPrefab = prefab.transform.Find("InteractionPrefab");
        Transform[] interactions = null;

        if (InteractionPrefab) interactions = InteractionPrefab.GetComponentsInChildren<Transform>();

        if (interactions != null)
        {
            for (int i = 0; i < interactions.Length; ++i)
            {
                string prefabPath = "Assets/Project Platformer/Prefabs/" + interactions[i].gameObject.name + ".prefab";
                UnityEngine.Object spawn = AssetDatabase.LoadAssetAtPath(prefabPath, typeof(GameObject));

                if (!spawn) Debug.Log("Prefab " + interactions[i].gameObject.name + " not found!");

                if (spawn != null)
                {
                    Transform oldTileObject = prefab.transform.Find(interactions[i].gameObject.name);
                    if (oldTileObject != null)
                    {
                        GameObject.DestroyImmediate(oldTileObject.gameObject);
                    }

                    GameObject spawnInstance = (GameObject)GameObject.Instantiate(spawn);
                    spawnInstance.name = spawn.name;
                    spawnInstance.name = spawnInstance.name + " - " + prefabNum;
                    prefabNum++;

                    spawnInstance.transform.parent = interactions[i].gameObject.transform;
                    if (dics.ContainsKey(interactions[i].gameObject))
                    {
                        string value = "";
                        IDictionary<string, string> temp;
                        dics.TryGetValue(interactions[i].gameObject, out temp);

                        if (temp.ContainsKey("DoorTeleport1:orientation"))
                        {
                            temp.TryGetValue("DoorTeleport1:orientation", out value);
                            if (value == "left")
                            {
                                interactions[i].GetChild(0).localScale = new Vector3(-1f, 1f, 1f);
                            }
                            interactions[i].GetChild(0).GetComponentInChildren<DoorTeleport1>().currentOrientation =
                                (DoorTeleport1.orientation)System.Enum.Parse(typeof(DoorTeleport1.orientation), value);
                        }

                        if (temp.ContainsKey("DoorTeleport1:mark"))
                        {
                            temp.TryGetValue("DoorTeleport1:mark", out value);
                            interactions[i].GetChild(0).GetComponentInChildren<DoorTeleport1>().mark = System.Int32.Parse(value);
                        }

                        if (temp.ContainsKey("DoorTeleport1:id"))
                        {
                            temp.TryGetValue("DoorTeleport1:id", out value);
                            interactions[i].GetChild(0).GetComponentInChildren<DoorTeleport1>().id = System.Int32.Parse(value);
                        }

                        if (temp.ContainsKey("Switch1:id"))
                        {
                            temp.TryGetValue("Switch1:id", out value);
                            interactions[i].GetChild(0).GetComponentInChildren<Switch1>().doorID = System.Int32.Parse(value);
                            

                            DoorTeleport1[] temp2 = prefab.transform.FindChild("InteractionPrefab").GetComponentsInChildren<DoorTeleport1>();
                            DoorTeleport1 chosen = null;

                            for (int j = 0; j < temp2.Length; j++)
                            {
                                if (temp2[j].id == System.Int32.Parse(value)) { chosen = temp2[j]; break; }
                            }

                            interactions[i].GetChild(0).GetComponentInChildren<Switch1>().door = chosen;
                        }
                    }

                    interactions[i].GetChild(0).position = interactions[i].position;
                    interactions[i].GetChild(0).parent = interactions[i].parent;


                    GameObject.DestroyImmediate(interactions[i].gameObject);

                }
            }
        }

        DoorTeleport1[] doorTeleports = prefab.GetComponentsInChildren<DoorTeleport1>();

        for (int i = 0; i < doorTeleports.Length; i++)
        {
            for (int j = 0; j < doorTeleports.Length; j++)
            {
                if (doorTeleports[i].mark == doorTeleports[j].mark && i != j)
                    doorTeleports[i].otherDoor = doorTeleports[j];
            }
        }


        Debug.Log("Interaction Prefabs imported successfully!");
    }
}
