﻿using UnityEngine;
using UnityEngine.UI;
using TeamUtility.IO;
using System.Collections;

public class ControlOptions : MonoBehaviour
{
    public string controlName = "";
    public Text buttonText = null;

    void OnEnable()
    {
        AxisConfiguration conf = InputManager.GetAxisConfiguration(PlayerID.One, controlName);
        buttonText.text = conf.positive.ToString();
    }

    public void Remap()
    {
        AxisConfiguration axisConfig = null;

        buttonText.text = "press any key";

        InputManager.StartKeyboardButtonScan((key, arg) => 
        {
            axisConfig = InputManager.GetAxisConfiguration(PlayerID.One, controlName);
            axisConfig.positive = (key == KeyCode.Backspace) ? KeyCode.None : key;
            buttonText.text = axisConfig.positive.ToString();
            return true;
        }, 10.0f, null);
    }
}
